" Wrap text
set wrap

" Number lines
set number relativenumber
set nu rnu

" Toggle NERDTree
nmap <F2> :NERDTreeToggle<CR>

call plug#begin('~/.vim/plugged')

" Syntax Highlighting
Plug 'sheerun/vim-polyglot'

" Auto closes ''
Plug 'jiangmiao/auto-pairs'

" Advanced Bottom bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" File Manager
Plug 'preservim/nerdtree'

call plug#end()

" Bar Theme
let g:airline_theme='term'
